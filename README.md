# deepAssign lite

Recursive object assign tool that focuses on quick-and-dirty way to handle options object in constructor or init process.

This is basically a remake of `jQuery.extend()`. So this inherits the same characterisics from it.
  
- The function only deep-copies if property's value is a plain object or an array.  
- The function _does not_ merge objects/arrays set to the same property in the `from` and `to` objects, but overwrites `to`'s value with the deep-copy of `from`'s value.

## Install

```
npm install --save-dev deep-assign-lite
```

## Usage

```javascript
// import as node module
const deepAssign = require('deep-assign-lite');

// or import as ES6 module
import deepAssign from '.../node_modules/deep-assign-lite/deepAssign.js';


const from = {
  foo: { x: 12, y: 34, z: ['i', 'j', 'k'] },
  bar: [ 'a', 'b', {c: 1, d: false} ],
  bam: () => {},
};
const to = {
  foo: {a: 0, b: 1},
  bar: [123, 456],
  baz: 'xyz',
};

const result = deepAssign(to, from);
// `result`` will be `to` itself with the properties copied from `from`:
{
  foo: { x: 12, y: 34,  z: ['i', 'j', 'k'] },  //=> deep copy of from.foo
  bar: [ 'a', 'b', {c: 1, d: false} ],         //=> deep copy of from.bar
  baz: 'xyz',
  bam: () => {},                               //=> shallow copy of from.bar
}

// Just like Object.assign and jQuery.extend, you can pass multiple `from` objects
const target = {};
const src1 = { foo: 123 },
const src2 = { bar: 456, baz: 789 };
const src3 = { bar: ['a', 'b'], bam: {x: 0, y: 0} };

deepAssign(target, src1, src2, src3);
// `target`` will be:
{
  foo: 123,
  bar: ['a', 'b'],
  baz: 789,
  bam: {x: 0, y: 0},
}
```

## License

- [MIT](./LICENSE)
