module.exports = {
  env: {
    es6: true,
    mocha: true,
  },
  extends: 'eslint:recommended',
  parserOptions: {
    sourceType: "module"
  },
};
