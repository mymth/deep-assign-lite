const { hasOwnProperty: has, toString: toStr } = Object.prototype;
const fn2str = Function.prototype.toString;
const objConstStr = fn2str.call(Object);

function isPlainObject(obj) {
  // @see https://stackoverflow.com/questions/9512661/why-use-tostring-to-typecheck-args-that-you-can-check-with-typeof
  if (!obj || toStr.call(obj) !== '[object Object]') {
    return false;
  }
  const proto = Object.getPrototypeOf(obj);
  if (!proto) {
    return true;
  }
  const typeOfConst = has.call(proto, 'constructor') ? typeof proto.constructor : undefined;
  return !(typeOfConst === 'function' && fn2str.call(proto.constructor) !== objConstStr);
}

/**
 * Deep-copies properties from one or more objects to another
 * This function only performs deep-copy on arrays and plain objects.
 * Also the function does not merge the same properties between the objects, but
 * overwrites with the deep-copied values.
 * e.g. deepAssign({A: {b: 1, c: 2}}, {A: {b: 4, d: 1}}) results {A: {b: 4, d: 1}}
 * (to.A is replaced with a deep-copy of from.A)
 * @param  {object} to target object where properties are copied into
 * @param  {...object} from object(s) to be copied from
 * @return {object} the target object
 */
export default function deepAssign(to, from) {
  if (arguments.length > 2) {
    return Array.from(arguments).slice(1).reduce((obj, arg) => deepAssign(obj, arg), to);
  }

  if (Array.isArray(from) || isPlainObject(from)) {
    Object.keys(from).forEach((key) => {
      const value = from[key];
      // the latter is to avoid recursive self-reference loop
      if (value === undefined || value === to) {
        return;
      }
      if (Array.isArray(value)) {
        to[key] = deepAssign([], value);
      } else if (isPlainObject(value)) {
        to[key] = deepAssign({}, value);
      } else {
        to[key] = value;
      }
    });
  }
  return to;
}
