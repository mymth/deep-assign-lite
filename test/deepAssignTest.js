import expect from 'unexpected';
import deepAssign from '../deepAssign.js';

describe('deepAssign()', function () {
  it('returns the `to` object', function () {
    const to = {foo: 123};
    const from = {bar: 456};

    expect(deepAssign(to, from), 'to be', to);
    expect(to, 'to equal', {foo: 123, bar: 456});
  });

  it('deep-copies plain object properties', function () {
    const to = {foo: 123};
    const from = {
      bar: {
        baz: 456,
        bam: {boo: 'abc', woo: 'def'},
      }
    };

    expect(deepAssign(to, from), 'to equal', {
      foo: 123,
      bar: {
        baz: 456,
        bam: {boo: 'abc', woo: 'def'},
      }
    });
    expect(to.bar, 'not to be', from.bar);
    expect(to.bar.bam, 'not to be', from.bar.bam);
  });

  it('deep-copies array properties', function () {
    const to = {foo: 123};
    const from = {
      bar: [456, ['abc', 'def']]
    };

    expect(deepAssign(to, from), 'to equal', {
      foo: 123,
      bar: [456, ['abc', 'def']]
    });
    expect(to.bar, 'not to be', from.bar);
    expect(to.bar[1], 'not to be', from.bar[1]);
  });

  it('does not copy undefined properties', function () {
    const to = {foo: 123};
    const from = {bar: undefined, baz: 0, bam: undefined};
    expect(deepAssign(to, from), 'to equal', {foo: 123, baz: 0});
  });

  it('does not copy recursive self-reference', function () {
    const to = {foo: 123};
    const from = {bar: to, baz: ''};
    expect(deepAssign(to, from), 'to equal', {foo: 123, baz: ''});
  });

  it('shallow-copies everything else', function () {
    const Test = function () {
      this.x = true;
    };
    const to = {foo: 123};
    const from = {
      bar: new Date(),
      baz: /\s/,
      bam: () => {},
      boo: new Test(),
      woo: null,
    };
    deepAssign(to, from);

    expect(to.bar, 'to be', from.bar);
    expect(to.baz, 'to be', from.baz);
    expect(to.bam, 'to be', from.bam);
    expect(to.boo, 'to be', from.boo);
    expect(to.woo, 'to be null');
  });

  it('overwrite the value if the same property is in from', function () {
    const to = {foo: {x: 0, y: 0}, bar: [456, 789]};
    const from = {foo: {a: 'am', p: 'pm'}, bar: [0]};
    deepAssign(to, from);

    expect(to.foo, 'to equal', {a: 'am', p: 'pm'});
    expect(to.foo, 'not to be', from.foo);
    expect(to.bar, 'to equal', [0]);
    expect(to.bar, 'not to be', from.bar);
  });

  it('accepts multiple `from`s', function () {
    const to = {foo: 123};
    const from1 = {bar: 456, baz: ['abc', 'def']};
    const from2 = {baz: ['xyz'], bam: {x: 7, y: 8}};
    const from3 = {bam: {z: 9}, boo: () => {}};

    expect(deepAssign(to, from1, from2, from3), 'to equal', {
      foo: 123,
      bar: 456,
      baz: ['xyz'],
      bam: {z: 9},
      boo: from3.boo,
    });
  });

  it('returns `to` untouched if `from` is not an array or a plain object', function () {
    const Test = function () {
      this.x = true;
    };
    const to = {foo: 123};

    expect(deepAssign(to, 456), 'to be', to);
    expect(to, 'to equal', {foo: 123});

    expect(deepAssign(to, new Test()), 'to equal', {foo: 123});
    expect(deepAssign(to), 'to equal', {foo: 123});
  });
});
